import Vue from 'vue'
import VueRouter from 'vue-router'
import Inicio from '../views/Inicio.vue'
import  regRecluso from '../views/registroRecluso.vue'
import  listReclusos from '../views/listaReclusos.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Inicio',
    component: Inicio
  },
  {
    path: '/registro-recluso',
    name: 'Registrar Recluso',
    component: regRecluso
  },
  {
    path: '/lista-reclusos',
    name: 'Lista de reclusos',
    component: listReclusos
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
